#!/bin/bash
cd /home/unimelb.edu.au/llafayette/repos/UniMelb/spartan-hpc-docs/
scp spartan:/home/lev/weather/softwarelist.txt docs/
scp spartan:/home/lev/weather/status.md .
git pull
sed -i -e "/## Spartan/ {r status.md" -e 'N}' docs/status_specs.md
sed -i '10,18d' docs/status_specs.md
git add docs/status_specs.md
git commit -a -m 'Weather report and module list'
git push
