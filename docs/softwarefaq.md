## Python

Q: How do I install my own Python packages?  
A: See our page [here](https://dashboard.hpc.unimelb.edu.au/software/#software-with-extensions)

## Nek5000

Q: Why does Nek5000 fail with an error "Vanishing jacobian"?  
A: There is a bug in older versions of Nek5000, which has been fixed with this [patch](https://github.com/Nek5000/Nek5000/commit/e3fb1225d76a844774b284f902a9d03e5f273247). Please pull the latest Nek5000 from Git.  

## Guppy  

Q: Why does `guppy_barcoder` fail to run on GPU nodes when the `--detect_mid_strand_barcodes` option is used?  
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Error: "Exception thrown in Barcoder worker thread: Too many sequences for GPU threads to contain! Will continue with next input file."  
A: This is a known bug in `guppy_barcoder`. To workaround the issue, explicitly specify the barcoding kit names (`--barcode_kits`) along with the  `--detect_mid_strand_barcodes` option.  
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; eg: `guppy_barcoder -i /data/guppy/abc -s out/ --trim_barcodes --detect_mid_strand_barcodes -x auto --barcode_kits "SQK-RBK004"`  

## SRA Toolkit

Q: Why do I get disk quota exceeded messages when using SRA Toolkit (e.g. prefetch)? I have lots of room left in my project area.  
A: By default, SRA Toolkit downloads files to a cache in your home directory (/home/username/ncbi/public/sra). To change this, [follow this guide](https://standage.github.io/that-darn-cache-configuring-the-sra-toolkit.html)  

## Tensorflow GPU  

Q: If I install Tensorflow GPU using Pip, what modules do I need to load from the module system to make it work?  
A: Tensorflow requires CUDA and CUDnn, as well as the installed pip package, to work. You can see which CUDA and CUDnn a Tensorflow Pip release requires [here](https://www.tensorflow.org/install/source#gpu)  

## FastX

Q: Why does FastX not work, giving me errors such as `Failed to connect to socket /tmp/dbus-QyT82wK6EG: Connection refused` and `Unable to determine failsafe session name. Possible causes: xfconfd isn't running (D-Bus setup problem); environment variable $XDG_CONFIG_DIRS is set incorrectly (must include "/etc"), or xfce4-session is installed incorrectly.`  
A: This is most likely due to you activating your Conda base env by default. Conda installs libraries and binaries which interfere with FastX. If you use Conda, please deactivate your base env upon login by running `conda config --set auto_activate_base false` 
