## Contacts

If you require support from the HPC Team, please contact us using the following forms
 
__**To request new software or changes to quota:**__

* UoM Staff: [http://go.unimelb.edu.au/7r96](http://go.unimelb.edu.au/7r96) 
 
* UoM Students: [http://go.unimelb.edu.au/7exr](http://go.unimelb.edu.au/7exr) 
 
* External users: [http://go.unimelb.edu.au/c5vr](http://go.unimelb.edu.au/c5vr) 
 
__**To report issues:**__
 
* UoM Staff: [http://go.unimelb.edu.au/kr96](http://go.unimelb.edu.au/kr96)
 
* UoM Students: [http://go.unimelb.edu.au/2exr](http://go.unimelb.edu.au/2exr)
 
* External users: [http://go.unimelb.edu.au/w5vr](http://go.unimelb.edu.au/w5vr)

__**Note - for external users, you need to register for an account to submit support requests. Please go to [https://unimelb.service-now.com/pr/?id=register](https://unimelb.service-now.com/pr/?id=register) to create your support account. The login for the support forms is not the same as your Spartan username/password **__


## Training

The HPC team offers several courses for new and experienced users. These include:

* Introduction to Linux and High Performance Computing
* Advanced Linux and Shell Scripting for High Performance Computing
* Introduction to Parallel Programming
* Introduction to GPU Programming
* Regular Expressions with Linux

We have also run specialist courses for researchers in bioinformatics, mechanical engineering, and neuroscience.

We *strongly* encourage researchers that are new to HPC to undertake training with us. It's free for University of Melbourne researchers. We run regular one-day courses on HPC, shell scripting, parallel programming, GPU programming, and regular expressions. Research Computing Services also offer training in a wide range of other digital tools to accelerate your research. 

We can also tailor a specific training program for you, for instance around a specific software package or discipline-based applications, if there is the demand. For example, we have previously run courses specifically for genomics and mechanical engineering.

Check [the Researcher Gateway](https://gateway.research.unimelb.edu.au/resources/platforms-infrastructure-and-equipment/research-infrastructure-platforms/research-computing-services#events) of when the next event is planned, along with the other training programs offered.

Finally, if you ever get stuck, please feel free to [contact HPC support](#contacts). We're here to help make your research more productive and enjoyable, and we'll do everything we can to help. 

