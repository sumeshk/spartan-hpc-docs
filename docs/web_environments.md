## FastX

FastX is a service which allows you to run graphical applications on Spartan, through a web browser. FastX submits a job to the batch system which executes a graphical environment (in this case, [XFCE](https://xfce.org/)). One can then connect and disconnect to the graphical environment as many times as one would like using your web browser while the batch job is still running.

To use it a user should open up a web-browser in their local system, go to [Spartan FastX](https://spartan-fastx.hpc.unimelb.edu.au) and log in with their Spartan username and password. Note that FastX is only available on the Uni network or via the Uni VPN.

Once connected to the graphical environment, one can start a terminal session, and load modules, run scripts etc to load graphical applications like MATLAB.

To start a session, click on the '+' in the top left hand corner
[![](https://dashboard.hpc.unimelb.edu.au/images/fastx1.png "Logo Title Text 1")](https://dashboard.hpc.unimelb.edu.au/guides/images/fastx1.png)

Follow with a single click on 'XFCE', and then click 'Launch'
[![](https://dashboard.hpc.unimelb.edu.au/images/fastx2.png "Logo Title Text 1")](https://dashboard.hpc.unimelb.edu.au/guides/images/fastx2.png)

Select the desired parameters for the session from the menu. Session Length, resources, or project execution can be specified. Note that larger sessions naturally take longer to be placed, particularly at times of heavy interactive use.
[![](https://dashboard.hpc.unimelb.edu.au/images/fastx6.png "Logo Title Text 1")](https://dashboard.hpc.unimelb.edu.au/guides/images/fastx6.png)

* Click outside the window to return to the main screen. Please note that it may take several seconds for a session to launch. One is limited to a single desktop session per user. 

* Click on the XFCE link to connect to a session
[![](https://dashboard.hpc.unimelb.edu.au/images/fastx3.png "Logo Title Text 1")](https://dashboard.hpc.unimelb.edu.au/guides/images/fastx3.png)

* Once connected, applications can be launched or scripts via the Terminal in the graphical session
[![](https://dashboard.hpc.unimelb.edu.au/images/fastx4.png "Logo Title Text 1")](https://dashboard.hpc.unimelb.edu.au/guides/images/fastx4.png)

* e.g. To launch MATLAB, load the MATLAB module, and then run the matlab command
[![](https://dashboard.hpc.unimelb.edu.au/images/fastx5.png "Logo Title Text 1")](https://dashboard.hpc.unimelb.edu.au/guides/images/fastx5.png)

## Open OnDemand

Open OnDemand is a HPC portal that provides web access to HPC resources, such a file management, command-line access, job management, and access to certain web-enabled applications (e.g., RStudio, Jupyter Notebooks). To access OpenOnDemand one needs to create a session from a Spartan login website with the user name and password.

[Login to Open OnDemand](https://spartan-ood.hpc.unimelb.edu.au/pun/sys/dashboard/batch_connect/sessions)

The following shows an example opening screen.

[![](https://dashboard.hpc.unimelb.edu.au/guides/images/openondemand1.png "Open OnDemand Opening Screen")](https://dashboard.hpc.unimelb.edu.au/guides/images/openondemand1.png)

Most of the menu items are self-explanatory. The "Files" menu item provide a GUI interface to a user's home directory. The "Jobs" menu has two parts, an "Active Jobs" application and a "Job Composer". The "Clusters" menu item provides an application for command-line access to Spartan in a web-browser. The "Interactive Apps" item provides links, replicated on a left hnad menu item, to a Jupyter Notebook server and an RStudio Server. In addition, there is a image for managing your interactive sessions. a help icon, a user icon, and a log out menu item.

### Files

The Open OnDemand File Explorer application provides a graphical representation of your home directory with a tree structure on the left side and file names, human readable file size and modification date on the right main division. Content can be navigated in the browser with the right main division changing accordingly.  A top menu division allows for directory navigation ("Go To"), a session to be opened in a browser-based terminal emulator, the creation of new files and directories, the capability to upload files, and options to show hidden files ("dotfiles") and the the file ownership and mode.

Files may be viewed, edited, renamed/moved, downloaded, and deleted. Because the session is running in the web-browser whatever file applications associations one has with the browser will be used on file. This is especially relevant for files in a binary format (.pdf, .docx, etc). For text files, Open OnDemand has its own viewer and editor in the browser. Note that the deletion rules are the same as being on the cluster; deleted is gone! 

Multiple files can be selected with shift and cursor navigation (either mouse-click or arrow keys), and there is an menu option for select/unselect all.

### Jobs

The Job Composer allows for the creation of some jobs with simple options, along with submitting such jobs, stopping them, and deleting them. The latter three are indicated by the play, stop, and trashcan icons. A new job can be composed from a default template or a specified path. Jobs will be in a OnDemand specified directory by default e.g., `/home/lev/ondemand/data/sys/myjobs/projects/default/1`

[![](https://dashboard.hpc.unimelb.edu.au/images/openondemand2.png "Open OnDemand File Explorer")](https://dashboard.hpc.unimelb.edu.au/guides/images/openondemand2.png)

Jobs can be edited with the Open OnDemand editor, and this will almost certainly be necessary to add modules etc. Consider using the [Job Composer](https://dashboard.hpc.unimelb.edu.au/forms/script_generator/) or make use of one of the many application-specific templates in `/usr/local/common`

### Clusters

The cluster menu option only has one item, SSH access to Spartan. This will provide a terminal emulator within the browser session. It will also make use of any .ssh config or passwordless SSH that has been previously installed. 

## Interactive Applications

The five interactive apps on Spartan's Open OnDemand are Jupyter Notebook, RStudio Server, Paraview, Matlab, Tensorboard. These work by starting up an interactive job on Spartan and forwarding the display to the web browser session. 

[![](https://dashboard.hpc.unimelb.edu.au/images/applications_ood.png "Open OnDemand Applications")](https://dashboard.hpc.unimelb.edu.au/guides/images/applications_ood.png)

When on starts on of these interactive apps some parameters need to be selected or entered to start the session.

For example, select version from the drop down menu on application page (Matlab, Paraview, RStudio-Server and Tensorboard):

[![](https://dashboard.hpc.unimelb.edu.au/images/MATLAB_dropdown_ood.png "MATLAB dropdown")](https://dashboard.hpc.unimelb.edu.au/guides/images/MATLAB_dropdown_ood.png)

To use Jupyter with GPU, add "fosscuda/2019b jupyter/1.0.0-python-3.7.4" in the Modules section.

[![](https://dashboard.hpc.unimelb.edu.au/images/Jupyter_modules_ood.png "Jupyter Modules")](https://dashboard.hpc.unimelb.edu.au/guides/images/Jupyter_modules_ood.png)

To use Jupyter from old module system, add "source /usr/local/module/spartan_old.sh" in the Commands to run before loading the modules section.

[![](https://dashboard.hpc.unimelb.edu.au/images/Jupyter_oldmodule_ood.png "Jupyter Old Modules")](https://dashboard.hpc.unimelb.edu.au/guides/images/Jupyter_oldmodule_ood.png)


To run software with GPU, put "gpgpu" in the Partition section, and "gpgpuresplat" (or other appropriate group with GPU permissions) in the QoS section, and change the "Number of GPUs" to be 1 or more

[![](https://dashboard.hpc.unimelb.edu.au/images/gpgpu_ood.png "GPGPU ood")](https://dashboard.hpc.unimelb.edu.au/guides/images/gpgpu_ood.png)

Rstudio-server allow user to load extra modules if needs in Load extra module section. eg rgdal/1.4-8-r-3.6.2

[![](https://dashboard.hpc.unimelb.edu.au/images/R_extra_modules_ood.png "R extra modules")](https://dashboard.hpc.unimelb.edu.au/guides/images/R_extra_modules_ood.png)

### Some known issues

Many operations will with Open OnDemand will be somewhat slower than direct access to Spartan as there is some overhead involved. However, there is some additional issues to be aware of.

* Filenames or directories with <a href="https://github.com/OSC/ood-fileexplorer/issues/198">XML or HTML</a> special characters (<. >, &) may cause issues with the display, resulting in an unusuable File Explorer application.

* Uploading very large files (that exceed the capacity of the temp directory) may fail or send a partial file without reporting an error. 

In all these cases it is recommended using a system that interacts with the file system directly (e.g., scp, rsync, FileZilla, WinSCP, etc)

* Opening larger files (multiple megabyte) in the file explorer can cause the right click to fail in Firefox.

* Attempting to open the tree's root menu in the File Explorer with a right-click in a new tab causes a navigation error.

* Setting the working directory on startup in RStudio does not work as expected. A recommended workaround is to `setwd` in RSTudio, and saved the start-up directory from RStudio in global options.

