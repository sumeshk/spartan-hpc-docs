Spartan hosts a GPGPU service, developed in conjunction with Research Platform Services, the Melbourne School of Engineering, Melbourne Bioinformatics, RMIT, La Trobe University, St Vincent's Institute of Medical
Research and Deakin University. It was funded through [ARC LIEF](http://www.arc.gov.au/linkage-infrastructure-equipment-and-facilities) grant LE170100200.

It consists of 72 nodes, each with four NVIDIA P100 graphics cards, which can provide a theoretical maximum of around 900 teraflops. Each P100 GPU has 12GB GPU RAM.


## Access
The GPGPU cluster is available to University researchers, as well as external institutions that partnered through the ARC LIEF grant.

Those with existing Spartan accounts will need to request access to use the GPGPU partition via a support ticket.

Those new to Spartan can signup for a user account at [https://dashboard.hpc.unimelb.edu.au/karaage/](https://dashboard.hpc.unimelb.edu.au/karaage/), as well as request a new project (or access to an existing one).


## Usage

The GPGPU cluster operates as per other Spartan partitions, and so the documentation on this page can be followed with respect to logging in, accessing software, and creating job scripts.

## How do I get access to GPUs?

Spartan includes a partition with GPUs (as well as a private `mig-gpu` partition and a private `deeplearn` partition). 

The `gpgpu` partition includes four Nvidia P100 GPUs per node. Each P100 GPU has 12GB GPU RAM.

They can be specified in your job script with `#SBATCH --partition gpgpu`.

You'll also need to include a generic resource request in your job script, for example `#SBATCH --gres=gpu:2` will request two GPUs for your job.

A range of GPU-accelerated software such as TensorFlow is available on Spartan [example](https://gitlab.unimelb.edu.au/hpc-public/spartan-examples/-/tree/master/TensorFlow), as well as CUDA for developing your own GPU applications [example](https://gitlab.unimelb.edu.au/hpc-public/spartan-examples/-/tree/master/GPU).

__N.B. The GPGPU partition is not automatically available to all Spartan users. You must request access to the GPGPU partition__ 

## Citation

If you are using the LIEF GPGPU cluster for a publication, you are required to include the following citation in the acknowledgements section of your paper:
 
`This research was undertaken using the LIEF HPC-GPGPU Facility hosted at the University of Melbourne.  This Facility was established with the assistance of LIEF Grant LE170100200.`

For University of Melbourne researchers, see our [Acknowledgements and Citations](https://gateway.research.unimelb.edu.au/resources/platforms-infrastructure-and-equipment/research-infrastructure-platforms/research-computing-services/services/acknowledgements-and-citations) page for more detail. 

## Use cases

[Tiffany Walsh - Deakin](https://dashboard.hpc.unimelb.edu.au/misc/GPGPU Use-Case Proforma-TRW.pdf)
