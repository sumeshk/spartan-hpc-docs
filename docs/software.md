## Environment Modules

In many computer environments only one version of an application is installed. On a shared system for researchers, this would not be a good idea. We need to have different versions of applications so that (a) users can be consistent in what version of a software application they are using, and relay that to others to help reproducibility and (b) users can access different features and developments which are introduced in different versions of a software application.

Spartan uses an environment modules system ([lmod](http://lmod.readthedocs.io/en/latest/)) to load and unload different applications to a user's $PATH, the variable which tracks what software is available to a user's environment. The environment modules system allows many different software applications to be installed on Spartan at once without interfering with each other.

To see what software applications and versions that a user current has in their $PATH the command `module list` can be used.

To check what's currently installed using the `module avail` command and a summary description of each application can be found through using `module spider`.

To load a particular application use the `module load` command. For example, to load GCC/9.2.0, use `module load gcc/9.2.0`. To unload a module's environment from a user's $PATH use the `module unload` command, for example, `module unload gcc/9.2.0`. To unload one module and load another use the `module switch` command, for example, `module switch gcc/9.2.0 gcc/10.1.0`.

Generally you shouldn't need to load modules from the login node, instead working on a compute node, either via an interactive session (launched with `sinteractive`), or from within your Slurm script.

## The New Modules System

The Spartan HPC has developed a new build system which includes the latest versions of software applications with newer toolchains. A feature of the new build system is that it explicitly protects users from mixing toolchains in the same job submission script. The Slurm Workload Manager and the LMod modules system does their best to change modules to the appropriate toolchain when a user mixes them in error, but an additional level of protect helps. When using the new modules system a view of modules will explicitly list only the modules that you have available for the toolchain in question. A user must load the appropriate toolchain to ensure that the right applications are available. 

If one tries to load a module, without loading a compiler first, an error will be generated:

```
$ module load openmpi/3.1.4  
Lmod has detected the following error:  Can't load openmpi/3.1.4
because it has more than one parent hierarchy, making this load ambiguous.  
Please load intel (for icc and ifort) or gcc of the following combinations
before loading this module:  
* gcc/8.3.0  
* ifort/2019.5.281-gcc-8.3.0  
* icc/2019.5.281-gcc-8.3.0   
* gcc/8.3.0 cuda/10.1.243  
```

Instead, load a compiler first.

`$ module load gcc/8.3.0; module load openmpi/3.1.4`

To switch back to the old modules system, run the following command:

`$ source /usr/local/module/spartan_old.sh`

## Software List and New Software

Every day a list of [software installed](/softwarelist.txt) on Spartan is regenerated from `module spider` and added to this page. This does not list all versions on the summary line. Other software can be installed on request.

If the software list doesn't include what one is after [Get in contact](https://dashboard.hpc.unimelb.edu.au/training_help/) with us and we can install it for you. Generally speaking, you should avoid compiling software on Spartan, unless you wrote it from scratch for your own use. By letting us handle it, we can make sure that:

* It works
* Software licenses are managed
* Code is compiled with the appropriate flags to maximize performance
* Other users can also make use of the software.

## Software with Extensions

### Python

There are multiple versions of Python installed on Spartan, which you can check using `module spider Python`. 

Common packages like numpy are already installed with some versions, but may be missing from others. 

* If a Python package is missing, we reommend you can install additional packages locally using `pip install --user <package name>` on the login node. Note that you will need to load a Python module using e.g. `module load gcccore/8.3.0; module load python/3.7.4` before running pip. 
This works well for pure-python packages, but you may encounter errors for those that link to other binary packages.  Alternatively, let us know, we can install it for you. 

* If a Python package exists in our module, and you need to update it, load the Python module e.g. `module load gcccore/8.3.0; module load python/3.7.4`, then run `pip install --user --ignore-installed <package name>` to update the package. 

### R

R's extensions are known as libraries. A lot of these have been installed by the Spartan HPC sysadmins when the main package itself is installed. To check what libraries are available, load the R module and enter that environment. Then run the command to display the available libraries.

```
$ module load r/3.6.2
$ R

R version 3.6.2 (2019-12-12) -- "Dark and Stormy Night"
..
> installed.packages(lib.loc = NULL, priority = NULL, noCache = FALSE, fields = NULL, subarch = .Platform$r_arch)
```

Note that the libraries available will differ for each version of R. 

However, sometimes a local (personal) library is required. Create a directory for local R libraries, invoke R, and install packages to that direcory. 

For example:

```
$ mkdir ~/R_libs
$ module load r/4.0.0
$ R
R version 4.0.0 (2020-04-24) -- "Arbor Day"
..
> install.packages("snow", repos="http://cran.r-project.org", lib="~/R_libs/")
..
> q();
$ echo 'R_LIBS_USER="~/R/libs"' >  $HOME/.Renviron
echo 'R_LIBS_USER="~/R_libs"' >  $HOME/.Renviron
$ ls ~/R_libs/
snow
```

Check to see that the library path has been added correctly. For example;

```
$ R
> .libPaths();
[1] "/home/lev/R_libs"                                               
[2] "/usr/local/easybuild-2019/easybuild/software/mpi/gcc/8.3.0/openmpi/3.1.4/r/4.0.0/lib64/R/library"
```

### MATLAB

In a job MATLAB can be invoked with a particular script using `matlab -nodisplay -nodesktop -r "run my_script.m"`. You may need to add particular working directories so MATLAB can find all the scripts necessary for your job.

To list the available MATLAB toolboxes use the "lstoolboxes" command to list the MATLAB toolboxes that are available (i.e. licensed) for use.

```
$ module load matlab/2020a
$ matlab
>> ver
```

Note that because MATLAB is particularly resource-heavy, it is prohibited to load it on the login nodes. 

To install packages for your own environment, follow the instructions from MathWorks on [Get and Manage Add-Ons](https://www.mathworks.com/help/matlab/matlab_env/get-add-ons.html). See also PACKAGES on Spartan at `/usr/local/common/MATLAB/PACKAGES`.


### COMSOL

COMSOL are able to run in GUI module via FastX by following command

```
$ comsol -3drend sw

```

For how to run FastX, Please see [How to use FASTX](https://dashboard.hpc.unimelb.edu.au/web_environments/#fastx).
