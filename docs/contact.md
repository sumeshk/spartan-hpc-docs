If you require support from the HPC Team, please contact us using the following forms
 
__**To request new software or changes to quota:**__

* UoM Staff: [http://go.unimelb.edu.au/7r96](http://go.unimelb.edu.au/7r96) 
 
* UoM Students: [http://go.unimelb.edu.au/7exr](http://go.unimelb.edu.au/7exr) 
 
* External users: [http://go.unimelb.edu.au/c5vr](http://go.unimelb.edu.au/c5vr) 
 

__**To report issues:**__
 
* UoM Staff: [http://go.unimelb.edu.au/kr96](http://go.unimelb.edu.au/kr96)
 
* UoM Students: [http://go.unimelb.edu.au/2exr](http://go.unimelb.edu.au/2exr)
 
* External users: [http://go.unimelb.edu.au/w5vr](http://go.unimelb.edu.au/w5vr)

__**Note - for external users, you need to register for an account to submit support requests. Please go to [https://unimelb.service-now.com/pr/?id=register](https://unimelb.service-now.com/pr/?id=register) to create your support account **__
 
