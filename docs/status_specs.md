## Spartan Daily Weather Report
### 20220307
* GPFS usage: 1563.51TB Free: 784.20TB (66%)
* Spartan utilisation on the physical partition is 100.00% node allocation, 94.68% CPU allocation.
* Spartan utilisation on the snowy partition is 100.00% node allocation, 86.77% CPU allocation.
* Total queued pending/queued on the public partitions: 1187
* Spartan utilisation on the GPGPU partition is 95.23% node allocation. Total queued/pending: 60
* GPUGPU card usage on the GPGPU partition is:  197 / 256 cards in use (76.95%)
* 2 nodes out.

### Current Usage
*How busy is Spartan today?*

<div id='divutilisation' style="width: 85%; margin: 0 auto;"></div>
<script>
  Plotly.d3.json('/jsondata/utilisation.json', function (error, data) {
    if (error) return console.warn(error);
    Plotly.newPlot("divutilisation", data.data, data.layout, {
      responsive: true
    });
  });
</script>

<ul class="accordion">
  <li>
<span class="accordion__title">How to Interpret</span>
<div class="accordion__hidden">
<ul>
<li>This plot provides indicates how many CPUs (cores) are in-use on Spartan.</li>
<li>Utilization may occasionally exceed 100% as resources are added/removed from service (utilization is always relative to the most recent available CPU count).</li>
<li>This data is acquired using the <code>sacct</code> command in Slurm to get a list of all recent jobs and their start/end times, and counting how many cores are allocated relative to total capacity.</li>
</ul>
</div>
</li>
</ul>

### Wait Time
*How long will my job take to start?*

<div id="divwaittime" style="width: 85%; margin: 0 auto;"></div>

<div class="container">
  <div class="row" style="width:50%; margin: 0 auto;">
    <div class="col-sm">
      Partition: <select id="partitionSelect" class="alt"></select>
    </div>
    <div class="col-sm">
      CPUs Requested: <select id="coreSelect" class="alt"></select>
    </div>
    <div class="col-sm">
      Wall Time: <select id="wallTimeSelect" class="alt"></select>
    </div>
  </div>
</div>

<br/>

<ul class="accordion">
  <li>
<span class="accordion__title">How to Interpret</span>
<div class="accordion__hidden">
<ul>
<li>This plot provides data on how long previous jobs have taken to start, which can be used as guidance on how long your job might take to start.</li>
<li>Note however that "Past performance is no guarantee of future results"; wait times can fluctuate quickly due to changes in usage or outages, and wait time could be considerably more or less than the historic average.</li>
<li>Daily averages are shown, but points may be missing for days where there were no jobs matching the selected characteristics.</li>
<li>This data is acquired using the <code>sacct</code> command in Slurm to get a list of all recent jobs and their start/end times.</li>
</ul>
</div>
</li>
</ul>


<script>

    window.onload = function() {

        Plotly.d3.json("/jsondata/wait_time.json" + '?' + Math.floor(Math.random() * 1000), function (data) {

            function assignOptions(options, selector) {
                $.each(options, function (val, text) {
                    selector.append(
                        $('<option></option>').val(text).html(text)
                    );
                });
            }

            function updatePlot() {
                var graphData = [
                    {   x: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['x'],
                        y: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['y'],
                        text: 'hours',
                        mode: 'markers',
                        hoverinfo: 'x+text+y',
                        marker: {
                            size: 10
                        },
                        name: 'Daily Average'
                    },
                    {
                        x: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['x'],
                        y: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['y_rolling_average'],
                        hoverinfo: 'none',
                        line: {
                            color: 'rgb(150, 150, 150)',
                            width: 1,
                            dash: 'dash'
                        },
                        name: 'Rolling 14d Mean'
                    }
                ];

                var layout = {
                    title: 'Job Wait Time',
                    width: 700,
                    height: 400,
                    yaxis: {
                        title: 'Hours',
                        hoverformat: '.2f hours'
                    },
                    legend: {
                        x: 0.5, 
                        y: 1.2,
                        orientation: 'h',
                        xanchor: 'center',
                    }
                };
                Plotly.newPlot('divwaittime', graphData, layout);
            }

            var partitionSelect = $('#partitionSelect');
            var coreSelect = $('#coreSelect');
            var wallTimeSelect = $('#wallTimeSelect');

            // Populate drop-down options
            assignOptions(data['options']['partitions'], partitionSelect);
            assignOptions(data['options']['cores'], coreSelect);
            assignOptions(data['options']['wall_times'], wallTimeSelect);

            // Attach listeners
            partitionSelect.change(updatePlot);
            coreSelect.change(updatePlot);
            wallTimeSelect.change(updatePlot);

            // Build initial plot
            updatePlot()

        });
    }


</script>


## Specifications

Spartan has a number of partitions available for general usage. A full list of partitions can be viewed with the command `sinfo -s`.

| Partition   | Nodes | Cores per node | Memory per node (MB) | Processor                                 | Peak Performance (DP TFlops) | Slurm node types |Extra notes                              |
|-------------|-------|------------|-------------|-------------------------------------------|------------------------------|------------------|-----------------------------------------|
| interactive | 3     | 72         | 745000       | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 15.2                         | physg5,avx512    | Max walltime of 2 days                  |
| long        | 2     | 72         | 745000       | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 10.1                         | physg5,avx512    | Max walltime of 90 days                 |
| physical    | 12    | 72         | 1519000      | Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz  | 58.1                        | physg4,avx512    |                                         |
|             | 70    | 72         | 745000       | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 354.8                        | physg5,avx512    |                                         |
| snowy       | 30    | 32         | 239000       | Intel(R) Xeon(R) CPU E5-2698 v3 @ 2.30GHz | 35.3                         | avx2             |                                         |
| gpgpu       | 73    | 24         | 234000       | Intel(R) Xeon(R) CPU E5-2650 v4 @ 2.20GHz | 176.6 (CPU) + 1358 (GPU)      | avx2             | 4 P100 Nvidia GPUs per node             |
| Total       |       |            |             |                                           | 582.2 (CPU) + 1358 (GPU)       |                  |                                         |

Total includes private partitions (including mig, msps2, and punim0396)
 
**Physical**

Each node is connected by high-speed 50Gb networking with 1.5 µsec latency, making this partition suited to multi-node jobs (e.g. those using OpenMPI).

You can constrain your jobs to use different groups of nodes (e.g. just the Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz nodes) by adding `#SBATCH --constraint=physg4` to your submit script

**GPGPU**

The GPGPU partition is funded through a LIEF grant. See [the GPU page](/gpu) for more details.

**AVX-512**

A number of nodes make use of the AVX-512 extended instructions. These include all of the nodes in the physical partition, and the login nodes. To submit a job on the physical partition that makes use of these instructions add `#SBATCH --constraint=avx512` in your submission script.

**Other Partitions**

There are also special partitions which are outside normal walltime constraints. In particular, `interactive` and `shortgpgpu` should be used for quick test cases; `interactive` has a maximum time of 2 days, and `shortgpgpu` has a maximum time constraint of one hour.

## Deeplearn

The deeplearn partition has been bought for use by Computing and Information Systems research staff and students

| Node name              | Nodes | Cores/node | Memory/node (MB) | Processor                                   | GPU Type | GPU Memory                      | Slurm node type                    |
|------------------------|-------|------------|-------------|---------------------------------------------|---------------|------------------|------------------------------------|
| spartan-gpgpu[072-075] | 4     | 28         | 234000       | Intel(R) Xeon(R) CPU E5-2690 v4 @ 2.60GHz   | v100 | 16GB GPU RAM per GPU     | dlg1                               |
| spartan-gpgpu[078-082] | 5     | 28         | 174000       | Intel(R) Xeon(R) Gold 6132 CPU @ 2.60GHz    | v100 | 16GB GPU RAM per GPU     | dlg2                               |
| spartan-gpgpu[086-088] | 3     | 24         | 175000       | Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz  | v100sxm2 | 32GB GPU RAM per GPU | dlg3                               |
| spartan-gpgpu[091-096] | 6     | 32         | 234000       | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz    | A100 | 40GB GPU RAM per GPU | dlg4                               |

To specify that your job should only run on a specific node type, add a constraint to your submission script. e.g. to specify the V100SXM2 nodes, add `#SBATCH --constraint=dlg3` to your submit script.

## Storage system

Spartan uses [IBM Spectrum Scale](https://www.ibm.com/support/knowledgecenter/en/SSFKCN/gpfs_welcome.html) (previously known as GPFS). This is a highly scalable, parallel and robust filesystem.

The total Spartan storage is broken up into 2 areas:

| Location     | Capacity | Disk type     |
|--------------|----------|---------------|
| /data/gpfs   | 2.34PB   | 10K SAS       |
| /data/scratch| 575TB    | Flash 	  |

/home is on the University's NetApp NFS platform, backed by SSD

