## Spartan Daily Weather Report
### 20220307
* GPFS usage: 1563.51TB Free: 784.20TB (66%)
* Spartan utilisation on the physical partition is 100.00% node allocation, 94.68% CPU allocation.
* Spartan utilisation on the snowy partition is 100.00% node allocation, 86.77% CPU allocation.
* Total queued pending/queued on the public partitions: 1187
* Spartan utilisation on the GPGPU partition is 95.23% node allocation. Total queued/pending: 60
* GPUGPU card usage on the GPGPU partition is:  197 / 256 cards in use (76.95%)
* 2 nodes out.
